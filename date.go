package goutil

import (
	"fmt"
	"time"
)

// 判断时间是当年的第几周
func WeekByDate(t time.Time) (string, int) {
	yearDay := t.YearDay()
	yearFirstDay := t.AddDate(0, 0, -yearDay+1)
	firstDayInWeek := int(yearFirstDay.Weekday())

	//今年第一周有几天
	firstWeekDays := 1
	if firstDayInWeek != 0 {
		firstWeekDays = 7 - firstDayInWeek + 1
	}
	var week int
	if yearDay <= firstWeekDays {
		week = 1
	} else {
		week = (yearDay-firstWeekDays)/7 + 2
	}
	return fmt.Sprintf("%d-%d", yearDay, week), week
}

// 计算两个时间的时间差（天数）
func DateSub(t1, t2 time.Time) int {
	d := t1.Sub(t2)
	return int(d.Hours() / 24)
}

// SplitDatetime 均分时间
func SplitDatetime(t1, t2 string) string {
	timeformat := "2006-01-02 15:04:05"
	tm1, _ := time.Parse(timeformat, t1)
	tm2, _ := time.Parse(timeformat, t2)

	elapsed := tm2.Sub(tm1)
	elapsed2 := elapsed / 2
	tm3 := tm1.Add(elapsed2)
	return tm3.Format(timeformat)
}

/**
 * @title				GetDateTime
 * @description			返回当前时间
 * @auth				MCHP		2021-10-10
 * @return				string		"2006-01-02 15:04:05"
 */
func GetDateTime() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

/**
 * @title				GetDateTime
 * @description			返回当前日期
 * @auth				MCHP		2021-10-10
 * @return				string		"2006-01-02"
 */
func GetDate() string {
	return time.Now().Format("2006-01-02")
}
