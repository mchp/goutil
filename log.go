package goutil

import (
	"fmt"
	"os"
)

// PrintAndWriteErrorLogFile 打印并写入错误日志
func PrintAndWriteErrorLogFile(txt string) {
	WriteErrorLogFile(txt)
	fmt.Printf("\n%s", txt)
}

// WriteErrorLogFile 错误日志
func WriteErrorLogFile(txt string) {
	curDir, _ := os.Getwd()
	filename := fmt.Sprintf("%s/log/error-%s.log", curDir, GetDate())
	var f *os.File
	if isexistpath(filename) {
		f, _ = os.OpenFile(filename, os.O_WRONLY|os.O_APPEND, 0666)
	} else {
		f, _ = os.Create(filename)
	}
	f.WriteString(fmt.Sprintf("\n[%s] - %s\n", GetDateTime(), txt))
}

// WriteSystemLogFile 系统日志
func WriteSystemLogFile(txt string) {
	curDir, _ := os.Getwd()
	filename := fmt.Sprintf("%s/log/system-%s.log", curDir, GetDate())
	var f *os.File
	if isexistpath(filename) {
		f, _ = os.OpenFile(filename, os.O_WRONLY|os.O_APPEND, 0666)
	} else {
		f, _ = os.Create(filename)
	}
	f.WriteString(fmt.Sprintf("\n[%s] - %s\n", GetDateTime(), txt))
}

// 判断文件夹是否存在，没有则创建
func CreateDir(path string) {
	curDir, _ := os.Getwd()
	dirpath := fmt.Sprintf("%s/log", curDir)
	if !isexistpath(dirpath) {
		os.Mkdir(dirpath, os.ModePerm)
	}
}

// isexistpath 判断路径是否存在
func isexistpath(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}
