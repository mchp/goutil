package goutil

import "log"

// FailOnError 一般的错误处理方法
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
