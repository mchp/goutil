package goutil

import "github.com/google/uuid"

// GetRandomUUID 生成随机的UUID
func GetRandomUUID() string {
	// V4 基于随机数
	u4, _ := uuid.NewRandom()
	return u4.String()
}
