package goutil

import (
	"fmt"
	"runtime"
)

// RunFuncName 获取运行此方法的方法名称
func RunFuncName() string {
	pc := make([]uintptr, 1)
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	return f.Name()
}

// CPanic 自定义异常类型
type CPanic struct {
	FuncName string      `json:"funcname"` // 方法名
	Object   interface{} `json:"object"`   // 对象
	Message  string      `json:"message"`  // 消息
	ErrorTxt string      `json:"errortxt"` // 错误文本
}

func NewCPanic(funcname string, object interface{}, msg string, errtxt string) *CPanic {
	return &CPanic{
		FuncName: funcname,
		Object:   object,
		Message:  msg,
		ErrorTxt: errtxt,
	}
}

func NewCPanicFromV(v interface{}) *CPanic {
	switch vobj := v.(type) {
	case string:
		s, _ := v.(string)
		return NewCPanic("", nil, s, "")
	case *CPanic:
		o, _ := v.(*CPanic)
		return o
	default:
		fmt.Println(vobj)
		t := CPanic{
			FuncName: "NewCPanicFromV",
			Object:   v,
			Message:  "v.(type) no match",
			ErrorTxt: v.(string),
		}
		return &t
	}
}

// ToString 输出
func (p *CPanic) ToString() string {
	txt := fmt.Sprintf("[%s][%s]: %s", p.FuncName, p.Message, p.ErrorTxt)
	return txt
}

// Print 打印
func (p *CPanic) Print() *CPanic {
	fmt.Printf("\n[%s]:%s\n", GetDateTime(), p.ToString())
	return p
}

// WriteErrorLogFile 写入错误日志
func (p *CPanic) WriteErrorLogFile() *CPanic {
	WriteErrorLogFile(p.ToString())
	return p
}

// PrintAndWriteErrorLogFile 打印并写入错误日志
func (p *CPanic) PrintAndWriteErrorLogFile() *CPanic {
	p.Print().WriteErrorLogFile()
	return p
}

// Panic 打印并写入错误日志之后，引发panic
func (p *CPanic) Panic() {
	p.PrintAndWriteErrorLogFile()
	panic(p)
}
